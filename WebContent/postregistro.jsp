<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<c:choose>
	<c:when test="${not empty sessionScope.usuario}">
		<c:redirect url="catalogo.jsp" />
	</c:when>
	<c:otherwise>
		<%
		String email = request.getParameter("email");
		if (email == null)
			response.sendRedirect("catalogo.jsp");
		else {
		%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Registro</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-4">
    <div class="container">
      <a class="navbar-brand" href="catalogo.jsp">Catálogo de Modelos a Escala</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
      <c:choose>
      <c:when test="${not empty sessionScope.customer}">
        
          <li class="nav-item active">
            <a class="nav-link" href="#">${sessionScope.customer}</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="cart.jsp">Carrito</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="logout">Cerrar sesion</a>
          </li>
        
        </c:when>
        <c:otherwise>
        	<li class="nav-item">
           		<a class="nav-link" href="login.jsp">Iniciar Sesion</a>
          	</li>
          	<li class="nav-item">
            	<a class="nav-link" href="registro.jsp">Registrarse</a>
          	</li>
		</c:otherwise>		
       </c:choose>
       </ul>
      </div>
    </div>
  	</nav>

	<div class="container-fluid">
  		<div class="row">
  			<div class="mx-auto bg-light p-5 rounded text-center">
				<h1>
					Bienvenido
				</h1>
				<p>
					Sólo queda un paso más para completar tu registro y tener acceso a
					todos nuestros Servicios, sigue las instrucciones que te hemos enviado
					a
					<%=email%></p>
				<p>Pulsa el botón de reenviar para que te volvamos a enviar el
					mensaje de confirmación:</p>
				<form action="reenvio" method="post">
					<input type="hidden" name="email" value="<%=email%>" />
					<p>
						<input class="btn btn-primary" type="submit" value="Reenviar" />
					</p>
				</form>
			</div>
		</div>
	</div>
</body>
</html>
<%
}
%>
</c:otherwise>
</c:choose>

