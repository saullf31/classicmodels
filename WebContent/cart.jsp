<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<c:if test="${empty sessionScope.customer}">
	<c:redirect url="catalogo.jsp" />
</c:if>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Carrito</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <style type="text/css">
    	tr,th,td{
    		padding: 10px;
    	}
    </style>
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-4">
    <div class="container">
      <a class="navbar-brand" href="catalogo.jsp">Catálogo de Modelos a Escala</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
      <c:choose>
      <c:when test="${not empty sessionScope.customer}">
        
          <li class="nav-item active">
            <a class="nav-link" href="#">${sessionScope.customer}</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="cart.jsp">Carrito</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="logout">Cerrar sesion</a>
          </li>
        
        </c:when>
        <c:otherwise>
        	<li class="nav-item">
           		<a class="nav-link" href="login.jsp">Iniciar Sesion</a>
          	</li>
          	<li class="nav-item">
            	<a class="nav-link" href="registro.jsp">Registrarse</a>
          	</li>
		</c:otherwise>		
       </c:choose>
       </ul>
      </div>
    </div>
  	</nav>

	<div class="container-fluid">
  		<div class="row">
  			<div class="mx-auto bg-light p-5 rounded text-center">
  			<h1>Carrito de la compra</h1>
  			<hr/>			
  				<c:choose>
					<c:when test="${empty sessionScope.cart}">
						<p>No se han añadido productos al carrito</p>
					</c:when>
					<c:otherwise>
						<table>
							<tr class="text-left"><th>Producto</th><th>Unidades</th>
							<th>Precio</th><th>Importe</th></tr>
							<c:forEach var="linea" items="${sessionScope.cart}">
								<tr>
									<td class="text-left px-2">${linea.value.productName}</td>
									<td>${linea.value.amount}</td>
									<td>${linea.value.price}</td>
									<td>${linea.value.amount * linea.value.price}</td>
									<td><a href="cart?c=${linea.value.productCode}&d=1" class="btn btn-primary">Eliminar del carro</a></td>
								</tr>
							</c:forEach>
						</table>
					</c:otherwise>		
				</c:choose>		
			</div>
		</div>
	</div>
	
	
</body>
</html>
